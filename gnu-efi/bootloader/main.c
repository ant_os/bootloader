#include <efi.h>
#include <efilib.h>
#include <elf.h>

#define PSF1_MAGIC0         0x36
#define PSF1_MAGIC1         0x04
#define BOOTLOADER_SUCCESS  0
#define BOOTLOADER_ERROR    -1

typedef unsigned long long size_t;

typedef struct {
    void*           base_address;
    size_t          buffer_size;
    unsigned int    width;
    unsigned int    height;
    unsigned int    pixels_per_scan_line;
} framebuffer_T;

typedef struct {
    unsigned char   magic[2];
    unsigned char   mode;
    unsigned char   char_size;
} psf1_header_T;

typedef struct {
    psf1_header_T*  psf1_header;
    void*           glyph_buffer;
} psf1_font_T;

typedef struct {
    framebuffer_T*          framebuffer;
    psf1_font_T*            psf1_font;
    EFI_MEMORY_DESCRIPTOR*  memory_map;
    UINTN                   memory_map_size;
    UINTN                   memory_map_descriptor_size;
    void*                   rsdp;
    void*                   ramdisk;
} boot_info_T;

enum LoggingTypes_E {
    LOGGING_SUCCESS,
    LOGGING_INFO,
    LOGGING_DEBUG,
    LOGGING_WARNING,
    LOGGING_ERROR,
    LOGGING_NONE
};

framebuffer_T g_framebuffer;


void log_string(CHAR16* string, enum LoggingTypes_E type) {
    switch(type) {
        case LOGGING_SUCCESS:
            Print(L"[ SUCCESS ] ");
            break;
        case LOGGING_INFO:
            Print(L"[  INFO   ] ");
            break;
        case LOGGING_DEBUG:
            Print(L"[  DEBUG  ] ");
            break;
        case LOGGING_WARNING:
            Print(L"[ WARNING ]");
            break;
        case LOGGING_ERROR:
            Print(L"[  ERROR  ]");
            break;
        default:
            break;
    }

    Print(string);
    Print(L"\n\r");
}

int initialize_gop() {
    EFI_GUID                        gop_guid    = EFI_GRAPHICS_OUTPUT_PROTOCOL_GUID;
    EFI_GRAPHICS_OUTPUT_PROTOCOL*   gop;
    EFI_STATUS                      status;

    status = uefi_call_wrapper(BS->LocateProtocol, 3, &gop_guid, NULL, (void**)&gop);
    if (EFI_ERROR(status)) {
        log_string(L"GOP not found", LOGGING_ERROR);
        return BOOTLOADER_ERROR;
    } else {
        log_string(L"GOP located", LOGGING_SUCCESS);
    }

    g_framebuffer.base_address          = (void*)gop->Mode->FrameBufferBase;
    g_framebuffer.buffer_size           = gop->Mode->FrameBufferSize;
    g_framebuffer.width                 = gop->Mode->Info->HorizontalResolution;
    g_framebuffer.height                = gop->Mode->Info->VerticalResolution;
    g_framebuffer.pixels_per_scan_line  = gop->Mode->Info->PixelsPerScanLine;

    return BOOTLOADER_SUCCESS;
}

EFI_FILE* load_file(EFI_FILE* directory, CHAR16* path, EFI_HANDLE image_handle, EFI_SYSTEM_TABLE* system_table) {
    EFI_FILE*                           loaded_file;
    EFI_LOADED_IMAGE_PROTOCOL*          loaded_image;
    EFI_SIMPLE_FILE_SYSTEM_PROTOCOL*    file_system;

    system_table->BootServices->HandleProtocol(image_handle, &gEfiLoadedImageProtocolGuid, (void**)&loaded_image);
    system_table->BootServices->HandleProtocol(loaded_image->DeviceHandle, &gEfiSimpleFileSystemProtocolGuid, (void**)&file_system);

    if (directory == NULL) {
        file_system->OpenVolume(file_system, &directory);
    }

    EFI_STATUS status = directory->Open(directory, &loaded_file, path, EFI_FILE_MODE_READ, EFI_FILE_READ_ONLY);
    if (status != EFI_SUCCESS) {
        return NULL;
    }
    return loaded_file;
}

psf1_font_T* load_psf1_font(EFI_FILE* directory, CHAR16* path, EFI_HANDLE image_handle, EFI_SYSTEM_TABLE* system_table) {
    EFI_FILE* font = load_file(directory, path, image_handle, system_table);
    if (font == NULL) return NULL;

    psf1_header_T * font_header;
    system_table->BootServices->AllocatePool(EfiLoaderData, sizeof(psf1_header_T), (void**)&font_header);
    UINTN size = sizeof(psf1_header_T);
    font->Read(font, &size, font_header);

    if (font_header->magic[0] != PSF1_MAGIC0 || font_header->magic[1] != PSF1_MAGIC1) {
        return NULL;
    }

    UINTN glyph_buffer_size = font_header->char_size * 256;
    if (font_header->mode == 1){
        glyph_buffer_size = font_header->char_size * 512;
    }

    void* glyph_buffer;
    {
        font->SetPosition(font, sizeof(psf1_header_T));
        system_table->BootServices->AllocatePool(EfiLoaderData, glyph_buffer_size, (void**)&glyph_buffer);
        font->Read(font, &glyph_buffer_size, glyph_buffer);
    }

    psf1_font_T* finished_font;
    system_table->BootServices->AllocatePool(EfiLoaderData, sizeof(psf1_font_T), (void**)&finished_font);

    finished_font->psf1_header = font_header;
    finished_font->glyph_buffer = glyph_buffer;
    return finished_font;
}

int memcmp(const void* a_ptr, const void* b_ptr, size_t n) {
    const unsigned char* a = a_ptr, *b = b_ptr;
    for (size_t i = 0; i < n; i++) {
        if (a[i] < b[i]) return -1;
        else if (a[i] > b[i]) return 1;
    }
    return 0;
}

UINTN strcmp(CHAR8* a, CHAR8* b, UINTN length) {
    for (UINTN i = 0; i < length; i++) {
        if (*a != *b) return 0;
    }
    return 1;
}

void* load_initial_ramdisk(EFI_FILE* directory, CHAR16* path, EFI_HANDLE image_handle, EFI_SYSTEM_TABLE* system_table) {
    EFI_FILE* ramdisk = load_file(directory, path, image_handle, system_table);
    if (ramdisk == NULL) return NULL;

    void* finished_ramdisk;

    EFI_FILE_INFO*  FileInfo    = LibFileInfo(ramdisk);
    UINTN           size        = FileInfo->FileSize;
    system_table->BootServices->AllocatePool(EfiLoaderData, size, (void**)&finished_ramdisk);
    ramdisk->Read(ramdisk, &size, finished_ramdisk);

    return finished_ramdisk;
}

EFI_STATUS efi_main (EFI_HANDLE image_handle, EFI_SYSTEM_TABLE* system_table) {
    InitializeLib(image_handle, system_table);

    log_string(L"bootloader started", LOGGING_SUCCESS);

    EFI_FILE* kernel_file = load_file(NULL, L"kernel.elf", image_handle, system_table);

    if (kernel_file == NULL) {
        log_string(L"Could not load 'kernel.elf'", LOGGING_ERROR);
        return EFI_LOAD_ERROR;
    } else {
        log_string(L"Loaded 'kernel.elf'", LOGGING_SUCCESS);
    }

    Elf64_Ehdr kernel_header;
    {
        UINTN file_info_size;
        EFI_FILE_INFO* file_info;
        kernel_file->GetInfo(kernel_file, &gEfiFileInfoGuid, &file_info_size, NULL);
        system_table->BootServices->AllocatePool(EfiLoaderData, file_info_size, (void**)&file_info);
        kernel_file->GetInfo(kernel_file, &gEfiFileInfoGuid, &file_info_size, (void**)&file_info);

        UINTN size = sizeof(kernel_header);
        kernel_file->Read(kernel_file, &size, &kernel_header);

    }

    if (memcmp(&kernel_header.e_ident[EI_MAG0], ELFMAG, SELFMAG) != 0 ||
        kernel_header.e_ident[EI_CLASS] != ELFCLASS64 ||
        kernel_header.e_ident[EI_DATA] != ELFDATA2LSB ||
        kernel_header.e_type != ET_EXEC ||
        kernel_header.e_machine != EM_X86_64 ||
        kernel_header.e_version != EV_CURRENT
        ){
        log_string(L"Kernel Format is bad", LOGGING_ERROR);
        return EFI_UNSUPPORTED;
    } else {
        log_string(L"Kernel header verified", LOGGING_SUCCESS);
    }

    Elf64_Phdr* kernel_program_headers;
    {
        kernel_file->SetPosition(kernel_file, kernel_header.e_phoff);
        UINTN size = kernel_header.e_phnum * kernel_header.e_phentsize;
        system_table->BootServices->AllocatePool(EfiLoaderData, size, (void**)&kernel_program_headers);
        kernel_file->Read(kernel_file, &size, kernel_program_headers);
    }

    for (Elf64_Phdr* program_header = kernel_program_headers;
         (char*)program_header < (char*)kernel_program_headers + kernel_header.e_phnum * kernel_header.e_phentsize;
         program_header = (Elf64_Phdr*)((char*)program_header + kernel_header.e_phentsize)) {

        switch (program_header->p_type) {
            case PT_LOAD: {
                int         pages       = (int)(program_header->p_memsz + 0x1000 - 1) / 0x1000;
                Elf64_Addr  segment     = program_header->p_paddr;
                system_table->BootServices->AllocatePages(AllocateAddress, EfiLoaderData, pages, &segment);

                kernel_file->SetPosition(kernel_file, program_header->p_offset);
                UINTN size = program_header->p_filesz;
                kernel_file->Read(kernel_file, &size, (void *) segment);
                break;
            }
        }
    }
    log_string(L"Kernel executable loaded", LOGGING_SUCCESS);

    void* init_rd = load_initial_ramdisk(NULL, L"init.rd", image_handle, system_table);
    if (init_rd == NULL) {
        log_string(L"Ramdisk invalid or not found", LOGGING_ERROR);
        return EFI_LOAD_ERROR;
    }
    log_string(L"Ramdisk loaded", LOGGING_SUCCESS);

    psf1_font_T* new_font = load_psf1_font(NULL, L"zap-light16.psf", image_handle, system_table);
    if (new_font == NULL) {
        log_string(L"Font invalid or not found", LOGGING_ERROR);
        return EFI_LOAD_ERROR;
    }
    log_string(L"Font loaded", LOGGING_SUCCESS);

    if(initialize_gop() == BOOTLOADER_ERROR) {
        return EFI_LOAD_ERROR;
    }

    EFI_MEMORY_DESCRIPTOR*  memory_map  = NULL;
    UINTN                   memory_map_size;
    UINTN                   memory_map_key;
    UINTN                   memory_map_descriptor_size;
    UINT32                  memory_map_descriptor_version;

    {
        system_table->BootServices->GetMemoryMap(&memory_map_size, memory_map, &memory_map_key, &memory_map_descriptor_size, &memory_map_descriptor_version);
        system_table->BootServices->AllocatePool(EfiLoaderData, memory_map_size, (void**)&memory_map);
        system_table->BootServices->GetMemoryMap(&memory_map_size, memory_map, &memory_map_key, &memory_map_descriptor_size, &memory_map_descriptor_version);
    }

    EFI_CONFIGURATION_TABLE*    config_table    = system_table->ConfigurationTable;
    void*                       rsdp            = NULL;
    EFI_GUID                    Acpi2TableGuid  = ACPI_20_TABLE_GUID;

    for (UINTN index = 0; index < system_table->NumberOfTableEntries; index++) {
        if (CompareGuid(&config_table[index].VendorGuid, &Acpi2TableGuid)) {
            if (strcmp((CHAR8*)"RSD PTR ", (CHAR8*)config_table->VendorTable, 8)) {
                rsdp = (void*)config_table->VendorTable;
            }
        }
        config_table++;
    }

    void (*KernelStart)(boot_info_T*) = ((__attribute__((sysv_abi)) void (*)(boot_info_T*)) kernel_header.e_entry);

    boot_info_T                                 boot_info;
    boot_info.framebuffer                   =   &g_framebuffer;
    boot_info.psf1_font                     =   new_font;
    boot_info.memory_map                    =   memory_map;
    boot_info.memory_map_size               =   memory_map_size;
    boot_info.memory_map_descriptor_size    =   memory_map_descriptor_size;
    boot_info.rsdp                          =   rsdp;
    boot_info.ramdisk                       =   init_rd;

    log_string(L"boot_info generated", LOGGING_SUCCESS);

    log_string(L"Leaving UEFI BootServices", LOGGING_INFO);
    system_table->BootServices->ExitBootServices(image_handle, memory_map_key);

    log_string(L"Starting Kernel...", LOGGING_INFO);
    KernelStart(&boot_info);

	return EFI_SUCCESS; // Exit the UEFI application
}
